import { createApp } from 'vue';
import { createPinia } from 'pinia';
import { createUi } from '@new-jira-clone/ui/src/install';
import dialog from '@new-jira-clone/ui/src/plugins/dialog';
import * as directives from '@new-jira-clone/ui/src/directives';
import i18nPlugin from '@new-jira-clone/libs/src/plugins/i18n';
import useTheme from '@new-jira-clone/ui/src/composables/utils/useTheme';
import WEditor from 'wysiwyg/WEditor';
// eslint-disable-next-line import/no-webpack-loader-syntax
import IconsSvgRaw from '!!raw-loader!@new-jira-clone/ui/dist/icons.svg';
import BaseImg from './components/BaseImg.vue';
import HostApp from './HostApp.vue';
import router from './router';
import { BgImageDirective } from './directives/bgImage.directive';

import '@/app/styles/reset.scss';
import '@new-jira-clone/ui/styles.scss';
import '@/app/styles/index.scss';

const app = createApp(HostApp);

app.use(i18nPlugin, {
  locale: 'en',
});

const pinia = createPinia();
const { initTheme } = useTheme();

initTheme();

// const WEditor = defineAsyncComponent(() => import('wysiwyg/WEditor'));

app.component('WEditor', WEditor);
app.component('BaseImg', BaseImg);

app.use(pinia);
app.use(createUi({ directives, svgSprite: { raw: IconsSvgRaw } }));
app.use(dialog);
app.use(router);

app.directive('bg-image', BgImageDirective);

app.mount('#app');
// window.sharedScope = __webpack_require__.S.default;
