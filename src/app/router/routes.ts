import { RouteRecordRaw } from 'vue-router';
import useChatRouter from 'Chat/router';
import useAuthRouter from 'Auth/router';
import DefaultLayout from '@/layouts/default.vue';
import AuthLayout from '@/layouts/auth.vue';

const { getRoutes: getChatRoutes } = useChatRouter();
const { getRoutes: getAuthRoutes } = useAuthRouter();

export const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: () => import('@/pages/home.vue'),
    meta: {
      layout: DefaultLayout,
    },
  },
  ...getAuthRoutes(AuthLayout),
  ...getChatRoutes(DefaultLayout),
];
