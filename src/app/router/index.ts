import { createRouter, createWebHistory } from 'vue-router';
import useAuthRouter from 'Auth/router';
import { routes } from './routes';

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeResolve((to) => {
  const { authGuard } = useAuthRouter();

  return authGuard(to);
});

export default router;
