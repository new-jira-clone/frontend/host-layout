import { onMounted, onUnmounted } from 'vue';

export default () => {
  const disableContextMenu = (event: any) => {
    if (event.target === null) return;

    if (
      event.target.nodeName !== 'INPUT'
      && event.target.type !== 'text'
      && event.target.nodeName !== 'TEXTAREA'
    ) {
      event.preventDefault();
    }
  };

  const initContextMenuListener = () => {
    document.body.addEventListener('contextmenu', disableContextMenu);
  };

  onMounted(() => {
    initContextMenuListener();
  });

  onUnmounted(() => {
    document.body.removeEventListener('contextmenu', disableContextMenu);
  });

  return {
    initContextMenuListener,
  };
};
