import type { Directive, DirectiveBinding } from 'vue';

interface Params {
  cdn: boolean;
  disable: boolean;
  url: string;
}

export const BgImageDirective: Directive = (
  el: HTMLElement,
  binding: DirectiveBinding<Params>,
): void => {
  const CDN_URL = process.env.VUE_APP_SERVER_URL;
  let prefix = CDN_URL;

  if (binding.value.disable) {
    // eslint-disable-next-line no-param-reassign
    el.style.backgroundImage = '';

    return;
  }

  if (binding.value.cdn) {
    const CDN_PREFIX_PATH = 'cdn/';

    prefix += CDN_PREFIX_PATH;
  }

  // eslint-disable-next-line no-param-reassign
  el.style.backgroundImage = `url(${prefix}${binding.value.url})`;
};
