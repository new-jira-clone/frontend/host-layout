FROM node:18-alpine AS build-stage
ARG VUE_APP_SERVER_URL=http://localhost:4000/
ARG VUE_APP_HOST='/'
ARG VUE_APP_WYSIWYG=http://localhost:3002
ARG VUE_APP_AUTH='http://localhost:3003'
ARG VUE_APP_CHAT='http://localhost:3005'
WORKDIR /auth
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build
FROM nginx:alpine as production-stage
COPY --from=build-stage /auth/dist /usr/share/nginx/html
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf 
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
