/* eslint-disable import/no-extraneous-dependencies */
const path = require('path');
const { defineConfig } = require('@vue/cli-service');
const { ModuleFederationPlugin } = require('webpack').container;
const webpack = require('webpack');

const dev = require('./package.json');

// TODO: посмотреть про /remoteEntry.js, является ли это хардкодом
const remoteWysiwyg = `${process.env.VUE_APP_WYSIWYG}/remoteEntry.js`;
const remoteAuth = `${process.env.VUE_APP_AUTH}/remoteEntry.js`;
const remoteChat = `${process.env.VUE_APP_CHAT}/remoteEntry.js`;
// const remoteFileUpload = 'http://localhost:3004/fileUploadRemoteEntry.js';

module.exports = defineConfig({
  publicPath: process.env.VUE_APP_HOST,
  transpileDependencies: true,
  css: {
    loaderOptions: {
      sass: {
        additionalData:
          '@use "./node_modules/@new-jira-clone/ui/src/styles/tools/index.scss" as tools;',
      },
    },
  },
  configureWebpack: {
    devtool: 'source-map',
    entry: path.resolve(__dirname, './src/index.ts'),
    optimization: {
      splitChunks: false,
    },
    resolve: {
      alias: {
        '@': path.resolve(__dirname, './src'),
        '@tools': path.resolve(
          __dirname,
          './node_modules/@new-jira-clone/ui/src/styles/tools/index.scss'
        ),
      },
    },
    plugins: [
      new webpack.EnvironmentPlugin({
        VUE_APP_SERVER_URL: JSON.stringify(process.env.VUE_APP_SERVER_URL),
        VUE_APP_SERVER_WS: JSON.stringify(process.env.VUE_APP_SERVER_WS),
      }),
      new ModuleFederationPlugin({
        name: 'host-layout',
        filename: 'remoteEntry.js',
        remotes: {
          wysiwyg: `wysiwyg@${remoteWysiwyg}`,
          Auth: `Auth@${remoteAuth}`,
          Chat: `Chat@${remoteChat}`,
          // fileUpload: `fileUpload@${remoteFileUpload}`,
        },
        shared: {
          ...dev.dependencies,
          vue: {
            eager: true,
            singleton: true,
            requiredVersion: dev.dependencies.vue,
          },
          pinia: {
            eager: true,
            singleton: true,
            requiredVersion: dev.dependencies.pinia,
          },
          'socket.io-client': {
            eager: true,
            singleton: true,
            requiredVersion: dev.dependencies['socket.io-client'],
          },
          'vue-router': {
            eager: true,
            singleton: true,
            requiredVersion: dev.dependencies['vue-router'],
          },
          '@new-jira-clone/libs/': {
            eager: true,
            singleton: true,
            strictVersion: false,
            requiredVersion: dev.dependencies['@new-jira-clone/libs'],
          },
          '@new-jira-clone/ui/': {
            eager: true,
            singleton: true,
            strictVersion: false,
            requiredVersion: dev.dependencies['@new-jira-clone/ui'],
          },
        },
      }),
    ],
  },
  devServer: {
    allowedHosts: 'auto',
    liveReload: true,
    hot: false,
    watchFiles: [
      path.resolve(__dirname, '../chat'),
      path.resolve(__dirname, '../auth'),
      path.resolve(__dirname, '../file-upload'),
    ],
    proxy: {
      'http://localhost:4020/api': {
        target: `${process.env.VUE_APP_SERVER_URL}api`,
        ws: true,
        changeOrigin: true,
      },
    },
  },
});
